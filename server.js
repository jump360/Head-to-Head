const express = require('express')
var bodyParser = require('body-parser')
const app = express()
var cfenv = require("cfenv")
var appEnv = cfenv.getAppEnv()
const winston = require('winston');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';
// app.use(bodyParser.urlencoded({ extended: false }))

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' })
  ]
});
 
//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
// 
if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }));
}

// parse application/json
app.use(bodyParser.json())
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
//Routes
var userRoute = require('./routes/userRoutes.js');
var projecRoute = require('./routes/projectRoute.js')



  //Accessing the routes
app.use('/user',userRoute);
app.use('/project', projecRoute);

app.get("/" , function(req , res) {
  console.log("Hellooo Cloudant");
  logger.log()
  logger.info("Hiii")
  res.send("Hello Developers");
})

app.listen(process.env.PORT || 8080, () => {
  console.log('started web process at Port 8080');
});