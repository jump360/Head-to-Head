var cred = require('./../credentials.js');
var Cloudant = require('@cloudant/cloudant');
var bcrypt = require('bcryptjs');
var HttpStatus = require('http-status-codes');
//console.log("Connecting to Cloudant database");
var cloudant = Cloudant(
    {
        url: cred.url,
        maxAttempt: cred.maxAttempt,
        plugins: cred.plugins
    }, function (err, cloudant) {
        if (err) {
            console.log(err)
            return res.status(HttpStatus.NOT_FOUND).send(err);
        }
        //console.log(cloudant)
    });

const db = cloudant.db.use("head-to-head-signup");
const BCRYPT_SALT_ROUNDS = 12;
module.exports = {
    signUp: function (req, res, next) {
        console.log("SignUp");
        var email = req.body.email;
        var name = req.body.name;
        var password = req.body.password;
        var status = 0;
        if (!email) {
            return res.status(HttpStatus.NOT_FOUND).send("Email Id Should not be blank");
        }
        if (!password) {
            return res.status(HttpStatus.NOT_FOUND).send("Password Should not be blank");
        }
        if (!name) {
            return res.status(HttpStatus.NOT_FOUND).send("Name Should not be blank");
        }

        db.find({ selector: { "email": email } }, function (err, doc) {
            //console.log(doc.docs);
            if (doc.docs.length) {
                console.log(doc.docs)
                console.log("Document Found")
                return res.status(HttpStatus.FORBIDDEN).send("Email Id Already Exist!!!!")
            }
            console.log("Creating USer");
            console.log("Email Id" + email + " " + "Password:" + password)
            bcrypt.hash(password, BCRYPT_SALT_ROUNDS)
                .then(function (hashedPassword) {
                    console.log(hashedPassword)
                    var signUp = {
                        email: email,
                        password: hashedPassword,
                        isLoggedin: false,
                        role: "user",
                        name : name,
                        count : 0
                    }

                    console.log(signUp);

                    return db.insert(signUp, function (err, body) {
                        if (err) {
                            console.log(err)
                            return res.send(err);
                        }
                        console.log(body);
                        status = 1;
                        return res.status(HttpStatus.OK).send({ message: "User is Registered Successfully", body: body })
                    })
                })
        })

    },
    signIn: async (req, res, next) => {
        console.log("SignIn");
        var email = req.body.email;
        var password = req.body.password;
        if (!email) {
            return res.status(403).send("Email Id Should not be blank");
        }
        if (!password) {
            return res.status(403).send("Password Should not be blank");
        }

        db.find({ selector: { "email": email } }, function (err, data) {
            if (err) {
                console.log(err)
            }
            if (!data.docs.length) {
                console.log("No Record Found of this User");
                return res.send("No Record Found of this User")
            }
            console.log(data)
            bcrypt.compare(password, data.docs[0].password).then((samePassword) => {
                if (!samePassword) {
                    return res.status(403).send("Password Doesnt Match");
                }
                db.insert({ _id: data.docs[0]._id, _rev: data.docs[0]._rev, email: data.docs[0].email, password: data.docs[0].password,name : data.docs[0].name,
                    role : data.docs[0].role, isLoggedin: true , count : data.docs[0].count},
                    function (err, data) {
                        if (err) {
                            console.log(err)
                            res.send(err)
                        }
                        console.log(data)
                    })
                return res.send({ id: data.docs, message: "Successfully Sign In" });

            }).catch(function (error) {
                console.log("Error authenticating user: ");
                console.log(error);
                next();
            });
        })

    },

    logout: async (req, res, next) => {
        console.log("Logout");
        var user_id = req.body.id;
        console.log("Doc Id", user_id);
        db.find({ selector: { "_id": user_id } }, function (err, data) {
            if (err) console.log(err);
            console.log(data.docs);
            if (data.docs.length == 0) {
                console.log("No Document Found")
                return res.send("No Document Found")
            }
            db.insert({ _id: user_id, _rev: data.docs[0]._rev, email: data.docs[0].email, password: data.docs[0].password, isLoggedin: false },
                function (err, document) {
                    if (err) {
                        console.log(err)
                        res.send(err)
                    }
                    console.log(data)
                    return res.status(HttpStatus.OK).send({ id: data.docs[0], message: "User Logged Out" });
                })




        })
    }

}



