const express = require('express')
var bodyParser = require('body-parser')
var cred = require('./../credentials.js');
var Cloudant = require('@cloudant/cloudant');
var HttpStatus = require('http-status-codes');
const Logger = require('logger-nodejs');
const logger = new Logger();
//console.log("Connecting to Cloudant database");
var cloudant = Cloudant(
    {
        url: cred.url,
        maxAttempt: cred.maxAttempt,
        plugins: cred.plugins
    }, function (err, cloudant) {
        if (err)
            console.log(err)
        //console.log(cloudant)
    });

var db = cloudant.db.use("project_headtohead");

module.exports = {

    //Adding New Project
    addproject: async function (req, res, next) {
        console.log("Adding Project");
        var projectName = req.body.projectname;
        var author = req.body.author;
        var department = req.body.department;
        var description = req.body.description;
        var role = req.body.role;
        var location = req.body.location;
        var email = req.body.email;


        if (!email) {
            return res.status(HttpStatus.NOT_FOUND).send("Email shouldn't be Blank");
        }
        if (!projectName) {
            return res.status(HttpStatus.NOT_FOUND).send("Project Name shouldn't be Blank");
        }
        if (!author) {
            return res.status(HttpStatus.NOT_FOUND).send("author shouldn't be Blank");
        }
        if (!department) {
            return res.status(HttpStatus.NOT_FOUND).send("department shouldn't be Blank");
        }
        if (!description) {
            return res.status(HttpStatus.NOT_FOUND).send("description shouldn't be Blank");
        }
        if (!role) {
            return res.status(HttpStatus.NOT_FOUND).send("role shouldn't be Blank");
        }
        if (!location) {
            return res.status(HttpStatus.NOT_FOUND).send("location shouldn't be Blank");
        }


        // //============ Adding the Project =========================
        var created_at = Math.round(new Date().getTime() / 1000);
        var project = {
            projectname: projectName,
            author: author,
            department: department,
            description: description,
            created_at: created_at,
            role: role,
            number_of_reviews: 0,
            revenue_total_points: 0,
            revenue_average_points: 0, /// number_of_revview
            speed_total_points: 0,
            speed_average_points: 0,
            cost_total_points: 0,
            cost_average_points: 0,
            brand_equity_total_points: 0,
            brand_equity_average_points: 0,
            brand_impact_total_points: 0,
            brand_impact_average_points: 0,
            total_weight : 0,
            location: location,
            isActive: true,
            email: email
        }
        console.log(project)
        logger.info(project);
        db.insert(project, function (err, body) {
            if (err) {
                console.log(err)
                res.status(HttpStatus.NOT_FOUND).send(err)
            }
            console.log(body);
            res.status(HttpStatus.OK).send({ doc_id: body.id, message: "Successfully added" });
        })



    },

    //Updating the Project
    updateproject: async function (req, res, next) {
        console.log("Updating The Project");
        var projectname = req.body.projectname;
        //var author = req.body.author;
        var department = req.body.department;
        var description = req.body.description;
        var projectid = req.body.projectid;
        var role = req.body.role;
        var location = req.body.location;

        var updated_at = Math.round(new Date().getTime() / 1000);
        if (!projectid) {
            return res.send("Enter the project id");
        }

        db.find({ selector: { "_id": projectid } }, function (err, data) {
            if (err) console.log(err);
            console.log(data.docs);
            if (data.docs.length == 0) {
                console.log("No Document Found")
                return res.send("No Document Found")
            }
            if (!projectname) {
                projectname = data.docs[0].projectname
            }
            if (!description) {
                description = data.docs[0].description;
            }
            // if (!author) {
            //     author = data.docs[0].author
            // }
            if (!department) {
                department = data.docs[0].department
            }
            if (!role) {
                role = data.docs[0].role;
            }
            if (!location) {
                location = data.docs[0].location;
            }
            db.insert({
                _id: projectid, _rev: data.docs[0]._rev,
                number_of_reviews: data.docs[0].number_of_reviews,
                revenue_total_points: data.docs[0].revenue_total_points,
                revenue_average_points: data.docs[0].revenue_average_points,
                speed_total_points: data.docs[0].speed_total_points,
                speed_average_points: data.docs[0].speed_average_points,
                cost_total_points: data.docs[0].cost_total_points,
                cost_average_points: data.docs[0].cost_average_points,
                brand_equity_total_points: data.docs[0].brand_equity_total_points,
                brand_equity_average_points: data.docs[0].brand_equity_average_points,
                brand_impact_total_points: data.docs[0].brand_impact_total_points,
                brand_impact_average_points: data.docs[0].brand_impact_average_points,
                projectname: projectname,
                author: data.docs[0].author,
                department: department,
                description: description,
                created_at: data.docs[0].created_at,
                role: role,
                location: location,
                isActive: data.docs[0].isActive,
                email: data.docs[0].email,
                updated_at : updated_at,
                total_weight : data.docs[0].total_weight
            }, function (err, data) {
                if (err) return console.log(err.message);
                console.log(data);

                res.status(HttpStatus.OK).send("Data has been Updated Successfully");
            })
            //})

        })

    },

    selectProject: async function (req, res, next) {
        var project_key = req.param('key');
        var data = [];
        console.log("Project Name" + project_key);
        db.view('project', 'BySelectProject', { include_docs: true }, function (err, graphdoc) {
            if (!err) {
                console.log("Db access successful - returning data");
                graphdoc.rows.forEach((docs) => {
                    if (docs.key === project_key) {
                        console.log(docs.value);
                        data.push({ key: docs.key, value: docs.value })
                    }
                    else {
                        console.log("No Documents Found");

                        // res.json("No Documents Found")
                    }

                    //data.push({key : docs.key , value : docs.value});
                })
                if (data.length != 0) {
                    res.json(data);
                }
                else {
                    res.json({ message: "No Document Found" });
                }
            }
            else console.log("failure -- error received --> ", err);
        });
    },

    //Getting all the Project List;
    getProjectList: async function (req, res, next) {
        console.log("Getting Project List");
        var dataArray = [];
        var email = req.body.email;
        console.log("Email ID \n", email);



        if (!email) {
            return res.status(HttpStatus.NOT_FOUND).send("Email shouldn't be Blank");
        }

        const database = cloudant.db.use("head-to-head-signup");
        database.find({ selector: { "email": email } }, function (err, data) {
            if (err) {
                console.log(err)
                res.status(HttpStatus.NOT_FOUND).send(err);
            }
            if (!data.docs.length) {
                console.log("No Record Found of this User");
                return res.status(HttpStatus.NOT_FOUND).send({ code: HttpStatus.NOT_FOUND, content: [] })
            }

            var author = data.docs[0].role;
            console.log("role \n", author);

            db.view('project', 'BySelectProject', { include_docs: true }, function (err, projectdata) {
                if (!err) {
                    console.log("Db access successful - returning data");

                    if (author == "user") {
                        projectdata.rows.forEach((docs) => {
                            //console.log(docs.value)
                            if (author == docs.value.author) {
                                if (docs.value.isActive == true && docs.value.email == email) {
                                    dataArray.push({ key: docs.key, projectname: docs.value.projectname });
                                }
                            }


                        })

                        res.json(dataArray);
                    }

                    //if author is Admin
                    if (author == "admin") {
                        projectdata.rows.forEach((docs) => {
                            //console.log(docs.value)

                            if (docs.value.isActive == true) {
                                dataArray.push({ key: docs.key, projectname: docs.value.projectname });
                            }


                        })

                        res.json(dataArrays);
                    }
                }
                else {
                    console.log("failure -- error received --> ", err);
                    return res.status(HttpStatus.NOT_FOUND).send(err);
                }

            });
        })
    },

    //getting all project Data
    getAllProjectData: async function (req, res, next) {
        console.log("Getting DAta");
        var dataArray = [];

        var email = req.body.email;
        console.log("Email ID \n", email);



        if (!email) {
            return res.status(HttpStatus.NOT_FOUND).send("Email shouldn't be Blank");
        }

        const database = cloudant.db.use("head-to-head-signup");
        database.find({ selector: { "email": email } }, function (err, data) {
            if (err) {
                console.log(err)
                res.status(HttpStatus.NOT_FOUND).send(err);
            }
            if (!data.docs.length) {
                console.log("No Record Found of this User");
                return res.status(HttpStatus.NOT_FOUND).send({ content: data.docs });
            }

            var author = data.docs[0].role;
            console.log("role \n", author);

            db.view('project', 'BySelectProject', { include_docs: true }, function (err, projectdata) {
                if (!err) {
                    console.log("Db access successful - returning data");
                    if (author == "user") {
                        projectdata.rows.forEach((docs) => {
                            console.log(docs.value)

                                if (docs.value.isActive == true && docs.value.email == email) {

                                    dataArray.push({ key: docs.key, data: docs.value });
                                }
                            

                        })
                        res.json(dataArray);
                    }

                    //If author is Admin
                    if (author == "admin") {
                        projectdata.rows.forEach((docs) => {
                            console.log(docs.value)
                            if (docs.value.isActive == true) {

                                dataArray.push({ key: docs.key, data: docs.value });
                            }


                        })
                        res.json(dataArray);
                    }

                }
                else console.log("failure -- error received --> ", err);
            });
        })
    },

    //Adding Paramters in Asses Project
    assessproject: async function (req, res, next) {

        console.log("Adding Assess Project Parameter");
        console.log(req.body)
        const projectKey = req.body.key;
        console.log(projectKey);
        const revenue = Number(req.body.revenue);
        const speed = Number(req.body.speed);
        const cost = Number(req.body.cost);
        const brandimpact = Number(req.body.brand_impact);
        const brandequity = Number(req.body.brand_equity);
        console.log("Revenue==>" + revenue)
        console.log("speed==>" + speed)
        console.log("cost==>" + cost)
        console.log("brandimpact==>" + brandimpact)
        console.log("brandequity==>" + brandequity)
        const total_weight = (revenue + speed + brandequity + cost + brandimpact) / 5;
        console.log("Average Weight", total_weight)
        var updated_at = Math.round(new Date().getTime() / 1000);

        //
        await db.find({ selector: { "_id": projectKey } }, function (err, result) {
            if (err) return console.log(err.message);
            console.log(result);
            var assessproject = {
                _id: projectKey, _rev: result.docs[0]._rev, revenue: revenue,
                speed: speed,
                cost: cost,
                brand_impact: brandimpact,
                brand_equity: brandequity,
                updated_at: updated_at,
                average_weight: total_weight,
                projectname: result.docs[0].projectname,
                author: result.docs[0].author,
                department: result.docs[0].department,
                description: result.docs[0].description,
                created_at: result.docs[0].created_at,
                isActive: result.docs[0].isActive
            }
            console.log("Data to be Updated", assessproject)
            db.insert(assessproject, function (err, data) {
                if (err) return console.log(err.message);
                console.log(data);
                res.send({ message: "Data has been added Successfully", data: assessproject })

            })
        })
    },

    //Getting project Data for Head to Head Review;
    h2hselection: async function (req, res, next) {
        console.log("Getting Data for Comparing Two Projects");
        var project1 = req.param('project1_id');
        var project2 = req.param('project2_id');
        console.log("Project 1", project1);
        console.log("Project 2", project2);

        let projectdata = {};
        //Finding Data for the First Project
        await db.find({ selector: { "_id": project1 } }, function (err, data) {
            if (err) return console.log(err.message);
            //console.log(data.docs)
            data.docs.forEach((project) => {
                //console.log(project)
                projectdata.project1 = project;
            })
            //Finding Data for the Second Project
            db.find({ selector: { "_id": project2 } }, function (err, data1) {
                if (err) return console.log(err.message);
                //console.log(data1.docs);
                data1.docs.forEach((project_1) => {
                    //console.log(project_1)
                    projectdata.project2 = project_1
                })
                console.log(projectdata)
                res.send(projectdata)
            })
        })
    },

    //Comparing Two Project and Estimating its cost and value And Checking whether the two project are reviewd by the particular or not
    headtoheadreview: async (req, res, next) => {
        var data = [];
        var flag = 0;
        var database = cloudant.db.use("headtohead_results");
        var email = req.body.email;
        if(!email) {
            return res.status(HttpStatus.NOT_FOUND).send("Email Cannot be null");
        }
        console.log("User/Admin EmailId ==> \n", email);
        //Getting the Project List
        await db.view('project', 'BySelectProject', { include_docs: true }, function (err, projectdata) {
            if (!err) {
                console.log("Getting the list of Project ID");
                projectdata.rows.forEach((docs) => {
                    //console.log(docs.value)
                    data.push(docs.id);
                });

                //CALLING RANDOMIZED FUNCTION
                randomselection(data, (err, callback) => {
                    if (err) console.error(err)
                    console.log("callback==", callback);
                    //console.log(callback.project1)

        ///====================== Checking whether the Results already exist or not =================================================

                    database.view('results', 'h2h_result', { include_docs: true }, function (err, resultdoc) {
                        if (!err) {
                            console.log("Db access successful - returning data");
                            // console.log(callback)
                            // console.log(resultdoc.rows.length);

                            if (resultdoc.total_rows == 0) {
                                //======================== Getting Data of Project ==========================================================================
                                let projectdata = {};
                                //Finding Data for the First Project
                                db.find({ selector: { "_id": callback.project1 } }, function (err, data) {
                                    if (err) return console.log(err.message);
                                    //console.log(data.docs)
                                    data.docs.forEach((project) => {
                                        //console.log(project)
                                        projectdata.project1 = project;
                                    })
                                    //Finding Data for the Second Project
                                    db.find({ selector: { "_id": callback.project2 } }, function (err, data1) {
                                        if (err) return console.log(err.message);
                                        //console.log(data1.docs);
                                        data1.docs.forEach((project_1) => {
                                            //console.log(project_1)
                                            projectdata.project2 = project_1
                                        })
                                        console.log(projectdata)
                                        res.status(HttpStatus.OK).send(projectdata)

                                    })
                                })
                            }
                            else {

                                //============================================================= if View length is greater than 0 ======================
                                // console.log(resultdoc.total_rows)
                                 resultdoc.rows.forEach((docs) => {
                                    console.log(docs.value)
                                    if ((docs.value.project1.project_id === callback.project1) && (docs.value.project2.project_id == callback.project2) &&(docs.value.email === email)) {
                                        console.log("Document match")
                                        //if document match then set flag to 1;
                                        flag = 1;
                                        res.status(HttpStatus.NOT_FOUND).send({ content: "Document Already Exist" })
                                    }
                                })

                                //console.log("Flagg==>", flag);
                                if (flag === 0) {

                                    //console.log("No Documents Found");
                                    //======================== Getting Data of Project ==========================================================================
                                    let projectdata = {};
                                    //Finding Data for the First Project
                                    db.find({ selector: { "_id": callback.project1 } }, function (err, data) {
                                        if (err) return console.log(err.message);
                                        //console.log(data.docs)
                                        data.docs.forEach((project) => {
                                            //console.log(project)
                                            projectdata.project1 = project;
                                        })
                                        //Finding Data for the Second Project
                                        db.find({ selector: { "_id": callback.project2 } }, function (err, data1) {
                                            if (err) return console.log(err.message);
                                            //console.log(data1.docs);
                                            data1.docs.forEach((project_1) => {
                                                //console.log(project_1)
                                                projectdata.project2 = project_1
                                            })
                                            console.log(projectdata)
                                            res.status(HttpStatus.OK).send(projectdata)

                                        })
                                    })
                                }

                            }
                        }
                        else console.log("failure -- error received --> ", err);

                    })


                })

            }
            else console.log("failure -- error received --> ", err);
        })
    },

    //Deleteing the Project(Document)
    deleteproject: async function (req, res, next) {
        console.log("Deleting the Project");
        var doc_id = req.body.doc_id;
        console.log("Document id==>" + doc_id);

        await db.get(doc_id, function (err, body) {
            if (err) {
                res.send(err);
                //return done(err);
            }
            console.log(body)
            console.log(body.projectname);
            db.insert({
                _id: doc_id, _rev: body._rev,
                number_of_reviews: body.total_review,
                revenue_total_points: body.total_revenue,
                revenue_average_points: body.avg_revenue,
                speed_total_points: body.total_speed,
                speed_average_points: body.avg_speed,
                cost_total_points: body.total_cost,
                cost_average_points: body.avg_cost,
                brand_equity_total_points: body.total_brand_equity,
                brand_equity_average_points: body.avg_brandequity,
                brand_impact_total_points: body.total_brand_impact,
                brand_impact_average_points: body.avg_brandimpact,
                updated_at: body.updated_at,
                
                projectname: body.projectname,
                author: body.author,
                department: body.department,
                description: body.description,
                created_at: body.created_at,
                
                role: body.role,
                location: body.location,
                isActive: false
            }, function (err, data) {
                if (err) return console.log(err.message);
                console.log(data);
                res.send("Data has been Updated Successfully");
            })
            //})

            //   db.destroy(doc_id, body._rev, function(error , result) {
            //         if(error) {
            //             console.log(error);
            //             res.send(error)
            //         }
            //         console.log(result);
            //         res.send("Document has been Deleted");
            //   })        
        })
    },

    //Adding Head to Head results
    headtoheadresult: async function (req, res, next) {
        console.log("Adding Head to Head Results");
        var database = cloudant.db.use("headtohead_results");
        var project_DB = cloudant.db.use("project_headtohead");
        var project1_obj = req.body.project1;
        var project2_obj = req.body.project2
        // var project2_author = req.body.project2_author;
        // var project2_description = req.body.project2_description;
        var email = req.body.email;

        if (!email) {
            return res.status(HttpStatus.NOT_FOUND).send("Email cannot be NULL");
        }

        if (!project1_obj) {
            return res.status(HttpStatus.NOT_FOUND).send("Project 1 Id shouldn't be Blank");
        }
        if (!project2_obj) {
            return res.status(HttpStatus.NOT_FOUND).send("Project 2 ID shouldn't be Blank");
        }
        // var project1_obj = {
        //     project_id: '5f1be7e25ddf18f627eaea2d25afb35f',
        //     project_number: 1,
        //     revenue: 96,
        //     speed: 0,
        //     cost: 60,
        //     brand_equity: 0,
        //     brand_impact: 81
        // }

        // var project2_obj = { project_id: '68d45d9cc3bbbf2c46a7bc11246b6516',
        //   project_number: 2,
        //   revenue: 0,
        //   speed: 31,
        //   cost: 0,
        //   brand_equity: 38,
        //   brand_impact: 0 }
        // var email = "headtohead@gmail.com"
        project1_obj.revenue = Math.abs(project1_obj.revenue);
        project1_obj.speed = Math.abs(project1_obj.speed);
        project1_obj.cost = Math.abs(project1_obj.cost);
        project1_obj.brand_equity = Math.abs(project1_obj.brand_equity);
        project1_obj.brand_impact = Math.abs(project1_obj.brand_impact);

        project2_obj.revenue = Math.abs(project2_obj.revenue);
        project2_obj.speed = Math.abs(project2_obj.speed);
        project2_obj.cost = Math.abs(project2_obj.cost);
        project2_obj.brand_equity = Math.abs(project2_obj.brand_equity);
        project2_obj.brand_impact = Math.abs(project2_obj.brand_impact);
        
        console.log(project1_obj);
        console.log(project2_obj);
        console.log(email)
        var userDB = cloudant.db.use('head-to-head-signup')

        //Adding the Project Review to Head to Head Result Database

        await database.insert({
            email: email,
            project1: project1_obj,
            project2: project2_obj
        }, function (err, insertedSnap) {
            if (err) {
                console.log(err);

            }
            console.log("Head to Head Result Inserted \n", insertedSnap.id);
        })
        // ==============================================================================================
        await userDB.find({ selector: { "email": email } }, function (err, doc) {
            //console.log(doc.docs);
            if (err) console.log(err);

            if (!doc.docs.length) {

                console.log("Document Not Found")
                return res.status(HttpStatus.NOT_FOUND).send("Email Id Doesn't Exist!!!!")
            }
            console.log(doc);
            var count = 0;
            if (doc.docs[0].count) {
                count = doc.docs[0].count + 1;
            }
            else {
                count = 1;
            }
            userDB.insert({
                _id: doc.docs[0]._id, _rev: doc.docs[0]._rev, email: doc.docs[0].email, password: doc.docs[0].password, name: doc.docs[0].name,
                role: doc.docs[0].role, isLoggedin: true, count: count
            },
                function (err, data) {
                    if (err) {
                        console.log(err)
                        res.send(err)
                    }
                    console.log("User doc Updated \n", data)
                })
        });

        // =================== Adding the Parameter to Project 1 ================;
        await project_DB.find({ selector: { "_id": project1_obj.project_id } }, function (err, data) {
            if (err) console.log(err);
            console.log(data.docs);
            if (data.docs.length == 0) {
                console.log("No Document Found")
                return res.send("No Document Found")
            }

            var total_review = 0, total_revenue = 0, total_cost = 0, total_speed = 0, total_brand_equity = 0, total_brand_impact = 0;
            var avg_revenue = 0, avg_cost = 0, avg_speed = 0, avg_brandequity = 0, avg_brandimpact = 0;
            if (data.docs[0].number_of_reviews) {
                total_review = data.docs[0].number_of_reviews + 1;
            }
            else {
                total_review = 1;
            }
            console.log("Total Review ==>", total_review)

            //Revenue 
            if (data.docs[0].revenue_total_points) {
                total_revenue = Math.abs(data.docs[0].revenue_total_points) + Math.abs(project1_obj.revenue);
                avg_revenue = Math.abs(total_revenue / total_review);
            }
            else {
                total_revenue = Math.abs(project1_obj.revenue);
                avg_revenue = total_revenue / total_review
            }

            //Cost
            if (data.docs[0].cost_total_points) {
                total_cost = Math.abs(data.docs[0].cost_total_points) + Math.abs(project1_obj.cost);
                avg_cost = total_cost / total_review;
            }
            else {
                total_cost = Math.abs(project1_obj.cost);
                avg_cost = total_cost / total_review;
            }

            //Speed
            if (data.docs[0].speed_total_points) {
                total_speed = Math.abs(data.docs[0].speed_total_points) + Math.abs(project1_obj.speed);
                avg_speed = total_speed / total_review
            }
            else {
                total_speed = Math.abs(project1_obj.speed);
                avg_speed = total_speed / total_review;
            }

            //Brand Equity
            if (data.docs[0].brand_equity_total_points) {
                total_brand_equity = Math.abs(data.docs[0].brand_equity_total_points) + Math.abs(project1_obj.brand_equity);
                avg_brandequity = total_brand_equity / total_review;
            }
            else {
                total_brand_equity = Math.abs(project1_obj.brand_equity);
                avg_brandequity = total_brand_equity / total_review;
            }

            //Brand Impact
            if (data.docs[0].brand_impact_total_points) {
                total_brand_impact = Math.abs(data.docs[0].brand_impact_total_points) + Math.abs(project1_obj.brand_impact);
                avg_brandimpact = total_brand_impact / total_review;
            }
            else {
                total_brand_impact = Math.abs(project1_obj.brand_impact);
                avg_brandimpact = total_brand_impact / total_review;
            }
            project_DB.insert({
                _id: data.docs[0]._id,
                _rev: data.docs[0]._rev,
                projectname: data.docs[0].projectname,
                author: data.docs[0].author,
                department: data.docs[0].department,
                description: data.docs[0].description,
                created_at: data.docs[0].created_at,
                role: data.docs[0].role,
                number_of_reviews: total_review,
                revenue_total_points: total_revenue,
                revenue_average_points: avg_revenue,
                speed_total_points: total_speed,
                speed_average_points: avg_speed,
                cost_total_points: total_cost,
                cost_average_points: avg_cost,
                brand_equity_total_points: total_brand_equity,
                brand_equity_average_points: avg_brandequity,
                brand_impact_total_points: total_brand_impact,
                brand_impact_average_points: avg_brandimpact,
                location: data.docs[0].location,
                isActive: data.docs[0].isActive,
                email: data.docs[0].email,
                total_weight : data.docs[0].total_weight
            }, function (err, updateProj) {
                if (err) {
                    res.status(HttpStatus.NOT_FOUND).send(err);
                    console.log(err)
                }

                console.log("Document Updated ==", updateProj.id)
            })
        })

        //=================== Adding the Parameter to Project 2 ================ ==============================;
        await project_DB.find({ selector: { "_id": project2_obj.project_id } }, function (err, data) {
            if (err) console.log(err);
            console.log(data.docs);
            if (data.docs.length == 0) {
                console.log("No Document Found")
                return res.send("No Document Found")
            }

            var total_review = 0, total_revenue = 0, total_cost = 0, total_speed = 0, total_brand_equity = 0, total_brand_impact = 0;
            var avg_revenue = 0, avg_cost = 0, avg_speed = 0, avg_brandequity = 0, avg_brandimpact = 0;
            if (data.docs[0].number_of_reviews) {
                total_review = data.docs[0].number_of_reviews + 1;
            }
            else {
                total_review = 1;
            }
            console.log("Total Review ==>", total_review)

            //Revenue 
            if (data.docs[0].revenue_total_points) {
                total_revenue = Math.abs(data.docs[0].revenue_total_points) + Math.abs(project2_obj.revenue);
                avg_revenue = total_revenue / total_review;
            }
            else {
                total_revenue = Math.abs(project2_obj.revenue);
                avg_revenue = total_revenue / total_review
            }

            //Cost
            if (data.docs[0].cost_total_points) {
                total_cost = Math.abs(data.docs[0].cost_total_points) + Math.abs(project2_obj.cost);
                avg_cost = total_cost / total_review;
            }
            else {
                total_cost = Math.abs(project2_obj.cost);
                avg_cost = total_cost / total_review;
            }

            //Speed
            if (data.docs[0].speed_total_points) {
                total_speed = Math.abs(data.docs[0].speed_total_points) + Math.abs(project2_obj.speed);
                avg_speed = Math.abs(total_speed / total_review)
            }
            else {
                total_speed = Math.abs(project2_obj.speed);
                avg_speed = total_speed / total_review;
            }

            //Brand Equity
            if (data.docs[0].brand_equity_total_points) {
                total_brand_equity = Math.abs(data.docs[0].brand_equity_total_points) + Math.abs(project2_obj.brand_equity);
                avg_brandequity = total_brand_equity / total_review;
            }
            else {
                total_brand_equity = Math.abs(project2_obj.brand_equity);
                avg_brandequity = total_brand_equity / total_review;
            }

            //Brand Impact
            if (data.docs[0].brand_impact_total_points) {
                total_brand_impact = Math.abs(data.docs[0].brand_impact_total_points) + Math.abs(project2_obj.brand_impact);
                avg_brandimpact = total_brand_impact / total_review;
            }
            else {
                total_brand_impact = Math.abs(project2_obj.brand_impact);
                avg_brandimpact = total_brand_impact / total_review;
            }
            project_DB.insert({
                _id: data.docs[0]._id,
                _rev: data.docs[0]._rev,
                projectname: data.docs[0].projectname,
                author: data.docs[0].author,
                department: data.docs[0].department,
                description: data.docs[0].description,
                created_at: data.docs[0].created_at,
                role: data.docs[0].role,
                number_of_reviews: total_review,
                revenue_total_points: total_revenue,
                revenue_average_points: avg_revenue,
                speed_total_points: total_speed,
                speed_average_points: avg_speed,
                cost_total_points: total_cost,
                cost_average_points: avg_cost,
                brand_equity_total_points: total_brand_equity,
                brand_equity_average_points: avg_brandequity,
                brand_impact_total_points: total_brand_impact,
                brand_impact_average_points: avg_brandimpact,
                location: data.docs[0].location,
                isActive: data.docs[0].isActive,
                email: data.docs[0].email,
                total_weight :  data.docs[0].total_weight
            }, function (err, updateProj) {
                if (err) {
                    console.log(err)
                    res.send(HttpStatus.NOT_FOUND).send(err);
                }
                console.log("Document Updated ==", updateProj.id)
                res.status(HttpStatus.OK).send({ content: "Projects are Updated Successfully" });
            })
        })
    },

    //Adding Department for the Project
    add_department: async (req, res, next) => {
        console.log("Adding New Department");
        var department = req.body.department;
        console.log("Department \n", department);
        var dept_Db = "project_department";
        var flag = 0;
        var database;
        await cloudant.db.list().then((List) => {
            //console.log(List)
            List.forEach((DBlist) => {
                console.log(DBlist)
                if (DBlist === dept_Db) {
                    //If present 
                    flag = 1;
                }

            })
        })
        console.log(flag)
        if (flag == 0) {
            database = cloudant.db.create(dept_Db).then((DbSnap) => {
                console.log("Database Created \n", DbSnap.ok)
            })
        }
        else {
            database = cloudant.db.use(dept_Db);
            console.log("Database Exist \n", dept_Db)
        }
        database.insert({ name: department }, function (err, data) {
            if (err) {
                console.log(err)
                res.status(HttpStatus.NOT_FOUND).send(err);
            }
            console.log(data)
            res.status(HttpStatus.OK).send({ message: "Department Added Successfully", docs: data });
        })
    },

    addrole: async (req, res, next) => {
        console.log("Adding Role");
        var role = req.body.role;
        console.log("Role \n", role);
        var role_db = "project_role";
        var flag = 0;
        var database;
        await cloudant.db.list().then((List) => {
            //console.log(List)
            List.forEach((DBlist) => {
                console.log(DBlist)
                if (DBlist === role_db) {
                    //If present 
                    flag = 1;
                }

            })
        })
        console.log(flag)
        if (flag == 0) {
            database = cloudant.db.create(role_db).then((DbSnap) => {
                console.log("Database Created \n", DbSnap.ok)
            })
        }
        else {
            database = cloudant.db.use(role_db);
            console.log("Database Exist \n", role_db)
        }
        database.insert({ name: role }, function (err, data) {
            if (err) {
                console.log(err)
                res.status(HttpStatus.NOT_FOUND).send(err);
            }
            console.log(data)
            res.status(HttpStatus.OK).send({ message: "Role Added Successfully", docs: data });
        })
    },

    addlocation: async (req, res, next) => {
        console.log("Adding Location");
        var location = req.body.location;
        console.log("Location \n", location);
        var location_db = "project_location";
        var flag = 0;
        var database;
        await cloudant.db.list().then((List) => {
            //console.log(List)
            List.forEach((DBlist) => {
                console.log(DBlist)
                if (DBlist === location_db) {
                    //If present 
                    flag = 1;
                }

            })
        })
        console.log(flag)
        if (flag == 0) {
            database = cloudant.db.create(location_db).then((DbSnap) => {
                console.log("Database Created \n", DbSnap.ok)
            })
        }
        else {
            database = cloudant.db.use(location_db);
            console.log("Database Exist \n", location_db)
        }



        await database.insert({ name: location }, function (err, data) {
            if (err) {
                console.log(err)
                res.status(HttpStatus.NOT_FOUND).send(err);
            }
            console.log(data)
            res.status(HttpStatus.OK).send({ message: "location Added Successfully", docs: data });
        })

    },

    updatedepartment: async (req, res, next) => {
        console.log("updating department");
        var doc_id = req.body.doc_id;
        console.log("Document ID", doc_id);
        var department = req.body.department;
        console.log("Department \n", department);
        var dept_Db = "project_department";
        var database = cloudant.db.use(dept_Db);
        database.find({ selector: { "_id": doc_id } }, function (err, data) {
            if (err) console.log(err);
            console.log(data.docs);
            if (data.docs.length == 0) {
                console.log("No Document Found")
                return res.status(HttpStatus.NOT_FOUND).send("No Document Found")
            }
            database.insert({
                _id: doc_id,
                _rev: data.docs[0]._rev,
                name: department

            }, function (err, insertedSnap) {
                if (err) {
                    console.log(err);
                    res.status(HttpStatus.NOT_FOUND).send(err)
                }
                console.log(insertedSnap.id);
                res.status(HttpStatus.OK).send({ content: "Data has been Updated Successfully", id: insertedSnap.id });
            })
        })
    },
    updaterole: async (req, res, next) => {
        console.log("updating role");
        var doc_id = req.body.doc_id;
        console.log("Document ID", doc_id);
        var role = req.body.role;
        console.log("Role \n", role);
        var role_db = "project_role";
        var database = cloudant.db.use(role_db);
        database.find({ selector: { "_id": doc_id } }, function (err, data) {
            if (err) console.log(err);
            console.log(data.docs);
            if (data.docs.length == 0) {
                console.log("No Document Found")
                return res.status(HttpStatus.NOT_FOUND).send("No Document Found")
            }
            database.insert({
                _id: doc_id,
                _rev: data.docs[0]._rev,
                name: role

            }, function (err, insertedSnap) {
                if (err) {
                    console.log(err);
                    res.status(HttpStatus.NOT_FOUND).send(err)
                }
                console.log(insertedSnap.id);
                res.status(HttpStatus.OK).send({ content: "Data has been Updated Successfully", id: insertedSnap.id });
            })
        })
    },
    updatelocation: async (req, res, next) => {
        console.log("Updating Location");
        var doc_id = req.body.doc_id;
        console.log("Document ID", doc_id);
        var location = req.body.location;
        console.log("Role \n", location);
        var location_db = "project_location";
        var database = cloudant.db.use(location_db);

        database.find({ selector: { "_id": doc_id } }, function (err, data) {
            if (err) console.log(err);
            console.log(data.docs);
            if (data.docs.length == 0) {
                console.log("No Document Found")
                return res.status(HttpStatus.NOT_FOUND).send("No Document Found")
            }
            database.insert({
                _id: doc_id,
                _rev: data.docs[0]._rev,
                name: location

            }, function (err, insertedSnap) {
                if (err) {
                    console.log(err);
                    res.status(HttpStatus.NOT_FOUND).send(err)
                }
                console.log(insertedSnap.id);
                res.status(HttpStatus.OK).send({ content: "Data has been Updated Successfully", id: insertedSnap.id });
            })
        })

    },
    getalldepartment: async (req, res, next) => {
        console.log("getting all dept");
        var dept_Db = "project_department";
        var database;
        database = cloudant.db.use(dept_Db);
        var deptArray = [];
        await database.view('Department', 'ByDept', { include_docs: true }, function (err, dataDocs) {
            if (err) console.log(err)
            console.log(dataDocs.total_rows)
            
            
                dataDocs.rows.forEach((dept) => {
                    deptArray.push({ id: dept.id, value: dept.value.name });
                    //console.log(dept.value)
                })
                console.log("Department Array " ,deptArray)
                res.status(HttpStatus.OK).send(deptArray);
            
            
        })
    },

    getallrole: async (req, res, next) => {
        console.log("Getting All Role");
        var role_Db = "project_role";
        var database;
        database = cloudant.db.use(role_Db);
        var roleArray = [];
        await database.view('Role', 'ByRole', { include_docs: true }, function (err, dataDocs) {
            if (err) console.log(err)
            if (dataDocs.total_rows == 0) {
                res.status(HttpStatus.NOT_FOUND).send("No Data Found");
            }
            dataDocs.rows.forEach((role) => {
                roleArray.push({ id: role.id, value: role.value.name });
                //console.log(dept.value)
            })
            console.log(roleArray)
            res.status(HttpStatus.OK).send(roleArray);
        })

    },
    getalllocation: async (req, res, next) => {
        console.log("Get All LOcation");
        var location_DB = "project_location";
        var database;
        database = cloudant.db.use(location_DB);
        var locaArray = [];
        await database.view('Location', 'ByLocation', { include_docs: true }, function (err, dataDocs) {
            if (err) console.log(err)
            if (dataDocs.total_rows == 0) {
                res.status(HttpStatus.NOT_FOUND).send("No Data Found");
            }
            dataDocs.rows.forEach((location) => {
                locaArray.push({ id: location.id, value: location.value.name });
                //console.log(dept.value)
            })
            console.log(locaArray)
            res.status(HttpStatus.OK).send(locaArray);
        })

    },

    geth2hreview: async (req, res, next) => {
        console.log("getting h2h review data");
        var database = cloudant.db.use("headtohead_results");
        var userDB = cloudant.db.use('head-to-head-signup');
        var userArray = [];
        var review = {};
        await userDB.view('Users', 'h2h_user', { include_docs: true }, function (error, userData) {
            if (error) console.log(error);
            userData.rows.forEach((user) => {
                console.log(user.value)
                userArray.push(user.value);
            })
        })
        await database.view('results', 'h2h_result', { include_docs: true }, function (err, h2hDocs) {
            if (err) console.log(err)
            userArray.forEach((userid) => {
                review[userid.email] = new Array();
                review[userid.email].count = userid.count;
                h2hDocs.rows.forEach((h2hData) => {

                    if (userid.email === h2hData.value.email) {

                        var counter = {
                            count: userid.count
                        }
                        h2hData.value.count = userid.count
                        review[userid.email].push(h2hData.value)
                        //console.log(review)
                    }
                    //console.log(h2hData.value);
                })
            })
            console.log("Review \n", review)
            res.status(HttpStatus.OK).send(review)

        })
    },

    deletedepartment: async (req, res, next) => {
        console.log("Deleteing department");


        var doc_id = req.body.doc_id;
        console.log("Doc ID \n", doc_id);
        if (!doc_id) {
            return res.send(HttpStatus.NOT_FOUND).send("Docid not found");
        }
        var db = cloudant.db.use('project_department');
        await db.get(doc_id, function (err, body) {
            if (err) {
                res.send(err)
                //return done(err);
            }
            console.log("bodyu",body);
            db.destroy(doc_id, function (error, result) {
                if (error) {
                    console.log(error);
                    res.send(error)
                }
                console.log(result);
                res.status(HttpStatus.OK).send({ content: "Document has been Deleted", id: result });
            })
        })
    },

    deleterole: async (req, res, next) => {
        console.log("Deleteing Role");


        var doc_id = req.body.doc_id;
        console.log("Doc ID \n", doc_id);
        if (!doc_id) {
            return res.send(HttpStatus.NOT_FOUND).send("Docid not found");
        }
        var db = cloudant.db.use('project_department');
        await db.get(doc_id, function (err, body) {
            if (err) {
                res.send(err)
                // done(err);
            }
            db.destroy(doc_id,  function (error, result) {
                if (error) {
                    console.log(error);
                    res.send(error)
                }
                console.log(result);
                res.status(HttpStatus.OK).send({ content: "Document has been Deleted", id: result });
            })
        })
    },

    deletelocation: async (req, res, next) => {
        console.log("Deleteing Location");


        var doc_id = req.body.doc_id;
        console.log("Doc ID \n", doc_id);
        if (!doc_id) {
            return res.send(HttpStatus.NOT_FOUND).send("Docid not found");
        }
        var db = cloudant.db.use('project_department');
        await db.get(doc_id, function (err, body) {
            if (err) {
                res.send(err)
                //return done(err);
            }
            db.destroy(doc_id, body._rev, function (error, result) {
                if (error) {
                    console.log(error);
                    res.send(error)
                }
                console.log(result);
                res.status(HttpStatus.OK).send({ content: "Document has been Deleted", id: result });
            })
        })
    }

}




var randomselection = async function (projectlist, callback) {
    console.log("Project List==>\n", projectlist);
    console.log(projectlist.length);

    // If the Project length is == 2 then no Random Selection Algo

    if (projectlist.length == 2 || projectlist.length == 1) {
        //console.log(projectlist[0]);
        callback(null, { project1: projectlist[0], project2: projectlist[1] });
    }
    else {
        //=======================Random Selection Explanation=============================
        //We add 1 because the current element can't be reselected and subtract 1 because one element has already been selected.
        //For example, an array of three elements (0, 1, 2). We randomly select the element 1. Now the "good" offset value 
        //are 0 and 1, with offset 0 giving the element 2 and offset 1 giving the element 0.
        //Note that this will give you two random elements with different INDEX, not with different VALUE!

        //-100 (means that Project A has earned 100 points for the specific metrics) and project B earned 0 points
        var elem1;
        var elem2;
        var elemListLength = projectlist.length;
        //console.log(elemListLength)

        var ix = Math.floor(Math.random() * elemListLength);
        //console.log(ix);
        elem1 = projectlist[ix];

        console.log(elem1)

        if (elemListLength > 1) {
            elem2 = projectlist[(ix + 1 + Math.floor(Math.random() * (elemListLength - 1))) % elemListLength];
        }
        console.log(elem2)
        callback(null, { project1: elem1, project2: elem2 });
    }

}

/*
nnumber_of_reviews=0
revenue_total_points=0
revenue_average_points=0 / number_of_revview
speed_total_points=0
speed_average_points=0
cost_total_points=0
cost_average_points=0
brand_equity_total_points=0
brand_equity_average_points=0
brand_impact_total_points=0
brand_impact_average_points=0 ---------------  

tal Weight = ((Revenue_Average * Revenue_Slider)+(Cost_Average * Cost_Slider) + (Speed_Average * Speed_Slider) + (Brand_Impact_Average * Brand_Impact_Slider) + (Brand_Equity_Average * Brand_Equity_Slider))/All_Slider_Sum*/