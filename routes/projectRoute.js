var express = require('express')
var app = express();
const router = express.Router();
var projectController = require('./../controller/projectController.js');

router.route('/addProject')
.post(projectController.addproject);

router.route('/updateProject')
.post(projectController.updateproject)

router.route('/selectProject')
.post(projectController.selectProject);

router.route('/getProjectList')
.post(projectController.getProjectList)

router.route('/getAllProjectData')
.post(projectController.getAllProjectData)

router.route('/assessProject')
.post(projectController.assessproject);

router.route('/deleteProject')
.post(projectController.deleteproject);

router.route('/H2H_Selection')
.post(projectController.h2hselection)

router.route('/headtoheadreview')
.post(projectController.headtoheadreview)

router.route('/headtoheadresult')
.post(projectController.headtoheadresult);

router.route('/addDepartment')
.post(projectController.add_department);

router.route('/addRole')
.post(projectController.addrole);

router.route('/addLocation')
.post(projectController.addlocation);

router.route('/updateDepartment')
.post(projectController.updatedepartment);

router.route('/updateRole')
.post(projectController.updaterole);

router.route('/updateLocation')
.post(projectController.updatelocation);

router.route('/getAllDept')
.post(projectController.getalldepartment);

router.route('/getAllRole')
.post(projectController.getallrole);

router.route('/getAllLoction')
.post(projectController.getalllocation);

router.route('/getH2HReview')
.post(projectController.geth2hreview);

router.route('/deleteDepartment')
.post(projectController.deletedepartment);

router.route('/deleteRole')
.post(projectController.deleterole);

router.route('/deleteLocation')
.post(projectController.deletelocation);



module.exports = router;