var express = require('express')
const router = express.Router();
var userController = require('./../controller/userController');

router.route('/signup')
.post(userController.signUp)

router.route('/signin')
.post(userController.signIn);

router.route('/logout')
.post(userController.logout)

module.exports = router
